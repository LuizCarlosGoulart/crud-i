import mongoose from 'mongoose'
import autoIncrement from 'mongoose-auto-increment'

const userSchema = mongoose.Schema({
    name: String,
    email: String,
    phone: String,
    birth: String,
})

autoIncrement.initialize(mongoose)
userSchema.plugin(autoIncrement.plugin, 'user')

const user = mongoose.model('user', userSchema)
export default user