import mongoose from "mongoose"

export async function configMongo() {
    const URL = `mongodb://user:123@crud-shard-00-00.nwi2v.mongodb.net:27017,crud-shard-00-01.nwi2v.mongodb.net:27017,crud-shard-00-02.nwi2v.mongodb.net:27017/?ssl=true&replicaSet=atlas-q85svy-shard-0&authSource=admin&retryWrites=true&w=majority`

    try {
        await mongoose.connect(URL)
        console.log('Database connected successfuly')
    } catch (error) {
        console.log('Error while connecting with the database', error)
    } 
}

