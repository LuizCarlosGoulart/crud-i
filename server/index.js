import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import bodyParser from "body-parser";
import { configMongo } from "./database/db.js";
import Routes from "./routes/route.js";

const app = express();
dotenv.config();
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use("/", Routes);

const PORT = 8000;

app.listen(PORT, () => {
  configMongo().then(async () => {
    console.log(`Server is running.      Port: ${PORT}`);
  });
});
