 O primeiro CRUD foi feito pensando em uma empresa com a necessidade de um banco de dados, no estilo 
de formulario a fim de gerenciar seus clientes, sendo visualmente acessível e muito prático de se manusear,
sendo amigável a pessoas que não tem tanto conhecimento de informatica

A solução para o primeiro CRUD foi utilizar o react para criar uma aplicação web visando deixar amigável
 visualmente o sistema, utilizando bibliotecas de componentes já estilizados para fazê-lo. O banco foi feito de
 forma simples, em estilo de formulário, devendo informar o nome, telefone, endereço, e data de nascimento, deixando
de forma mais objetiva e intuitiva utilizando botões estilizados.
