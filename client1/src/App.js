import './App.css'
// Components
import Start from './components/Start'
import AddUser from './components/AddUser'
import AllUsers from './components/AllUsers'
import EditUser from './components/EditUser'
import NavBar from './components/NavBar'

import {BrowserRouter, Routes, Route} from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
      <NavBar/>
      <Routes>
        <Route path='/' element={<Start/>}/>
        <Route path='/all' element={<AllUsers/>}/>
        <Route path='/add' element={<AddUser/>}/>
        <Route path='/edit/:id' element={<EditUser/>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default App