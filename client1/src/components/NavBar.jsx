import { AppBar, Toolbar, styled } from "@mui/material";
import { NavLink } from "react-router-dom";

const Header = styled(AppBar)`
  background: black;
`;

const Tabs = styled(NavLink)`
  font-size: 20px;
  margin-right: 10px;
  color: inherit;
  text-decoration: none;
`;

const NavBar = () => { //euae
  return (
    <Header position="static">
      <Toolbar>
        <Tabs to="/"> Start </Tabs>
        <Tabs to="/all"> All Users </Tabs>
        <Tabs to="/add"> Add User </Tabs>
      </Toolbar>
    </Header>
  );
};

export default NavBar;
