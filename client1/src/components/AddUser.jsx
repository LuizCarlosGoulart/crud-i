import {useState} from 'react'
import { FormControl, FormGroup, InputLabel, Input , Typography, styled, Button} from "@mui/material"
import {addUser} from '../service/api'
import {BirthDate} from '../components/birth'
import {useNavigate} from 'react-router-dom'

const Container = styled(FormGroup)`
    width: 50%;
    margin: 5% auto 0 auto;
    & > div {
        margin-top: 20px;
    }
    Button {
        border-radius: 20px;
    }
`

const defaultValue = {
    name: '',
    email: '',
    phone: '',
    birth: '',
}

const AddUser = () => {

    const [user, setUser] = useState(defaultValue);

    const navigate = useNavigate

    const onValueChange = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value})
    }

    const addUserDetails = async () => {
        await addUser(user)
        navigate('/all')
    }

    return (
        <Container>
            <Typography variant="h4"> Adicionar </Typography>
            <FormControl>
                <InputLabel>Nome</InputLabel>
                <Input onChange= {(e) => onValueChange(e)} name='name'/>
            </FormControl>
            <FormControl>
                <InputLabel>E-mail</InputLabel>
                <Input onChange= {(e) => onValueChange(e)} name='email'/>
            </FormControl>
            <FormControl>
                <InputLabel>Telefone</InputLabel>
                <Input onChange= {(e) => onValueChange(e)} name='phone'/>
            </FormControl>
            <FormControl>
                <InputLabel></InputLabel>
                <BirthDate setValue={setUser} value = {user}/>
            </FormControl>
            <FormControl>
                <Button variant="contained" onClick={() => addUserDetails()}> Adicionar</Button>
            </FormControl>
        </Container>
    )
}

export default AddUser