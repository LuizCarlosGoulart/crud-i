import * as React from 'react';
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';
import Stack from '@mui/material/Stack';
import { ptBR } from "date-fns/locale";

export function BirthDate({setValue, value}) {

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}
    adapterLocale={ptBR}>
      <Stack spacing={3}>
        <MobileDatePicker
          label="Data de Nascimento"
          value={value?.birth}
          onChange={(date) => setValue({...value, birth: date})}
        
          renderInput={(params) => <TextField {...params} />}
        />
      </Stack>
    </LocalizationProvider>
  );
}