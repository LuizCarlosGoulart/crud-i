import {useEffect, useState} from 'react'
import {Table, TableHead, TableBody, TableRow, TableCell, styled, Button} from '@mui/material'
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete'
import {getUsers, deleteUser} from '../service/api'
import {Link} from 'react-router-dom'

const StyledTable = styled(Table)`
    width: 60%;
    margin: 50px auto 0 auto;
`

const THead = styled(TableRow)`
    background: #000;
    & > th {
        color: #fff;
        font-size: 15px;
    }
`

const TBody = styled(TableRow)`
    & > td {
        font-size: 15px;
        padding: 5px 10px;
    }
    Button {
        font-size: 10px;
        background-color: #C92429;
    }
    Button:hover {
        background-color: #AB1E22;
        color: white;
      }

    a {
        font-size: 10px;
    }
`

const AllUsers = () => {

    const [users, setUsers] = useState([])

    useEffect(() => {
        getAllUsers()
    }, [])

    const getAllUsers = async () => {
        let response = await getUsers()
        setUsers(response.data)
    }

    const deleteUserDetails = async (id) => {
        await deleteUser(id)
        getAllUsers()
    }

    return (
        <StyledTable>
            <TableHead>
                <THead>
                    <TableCell> ID </TableCell>
                    <TableCell> Nome </TableCell>
                    <TableCell> E-mail </TableCell>
                    <TableCell> Telefone </TableCell>
                    <TableCell> Data de Nascimento </TableCell>
                    <TableCell> </TableCell>
                </THead>
            </TableHead>
            <TableBody>
                {
                    users.map(user => (
                        <TBody key={user._id}>
                            <TableCell> {user._id} </TableCell>
                            <TableCell> {user.name} </TableCell>
                            <TableCell> {user.email} </TableCell>
                            <TableCell> {user.phone} </TableCell>
                            <TableCell> {user.birth.slice(0,-14)} </TableCell>
                            <TableCell>
                                <Button variant="contained" style={{marginRight:10}} component={Link} to={`/edit/${user._id}`}>
                                    <EditIcon/>
                                    </Button>
                                <Button variant="contained"  color="secondary" onClick={() => deleteUserDetails(user._id)}>
                                    <DeleteIcon/>
                                </Button>
                            </TableCell>
                        </TBody>
                    ))
                }
            </TableBody>
        </StyledTable>
    )
}

export default AllUsers